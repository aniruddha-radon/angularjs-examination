**ANGULAR JS EXAMINATION**

1. Create a page with two partitions. The partition on the left will have a search bar.
Below the search bar there will be menu items (restaurant menu). Each item in
the menu will have a ‘price’ and a '+' button. On clicking the plus button the item
will be added in the partition on the right side of the page. Each item on the right
side, will have a '-' button which will delete the item from the list. The total price of
the selected items should be shown in the right bottom corner. The search bar
should be functional and should filter according to name.


2. Create a page where employees are displayed in a table list, the employee list
data will be fetched from http://dummy.restapiexample.com/api/v1/employees.
On clicking an employee, a pop up will open with the employee details with the
option of deleting the user. If the user is deleted, the background color of the
employees row will be grayed out. The user should be able to un-delete an
employee.


3. display a list of 12 (twelve) hotels with the following details - Name, location,
rating (out of 5), price (per night), available rooms. The hotels will be displayed in
a table, where only 3 hotels are shown at one time. Below the table there will be
‘previous’ and ‘next’ buttons. The previous button will be disabled if the first three
hotels are shown. A click on the next button will show the next 3 hotels. The next
button will be disabled when the last three hotels are being shown. A click on the
previous button will show the previous 3 hotels. Above the table create a filter
with filters on Name, rating (drop down) and price. The hotels should be filtered
based on the filter values.


4. Create a page with 2 partitions. Both partitions will have 1 drop down at the top
with the label ‘select your pokemon’. The drop down will have a list of 10
pokemons. When both the pokemons are selected, the user will be shown 4
attacks of the pokemon below the drop downs and both pokemons will be given a
health of 50. On clicking an attack, the power of the attack should be calculated
and the health of the other pokemon will be deducted. The first pokemon to reach
0 health will be the loser and the background will become gray and the winner
pokemons background will become green.